#pragma once

#include <iostream>
#include <QString>
#include <QWindow>
#include <QLineEdit>
#include <QLabel>
#include <QRadioButton>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>

class InputDialog : public QWidget {
	Q_OBJECT

	QLabel *m_labelNumber1;
	QLabel *m_labelNumber2;

	QLineEdit *m_editNumber1;
	QLineEdit *m_editNumber2;

	QRadioButton *m_radioSum;
	QRadioButton *m_radioGCD; // gcd - greatest common divisor
	QRadioButton *m_radioLCM; // lcm - least common multiplier

	QVBoxLayout *m_mainVerticalLayout;
	QHBoxLayout *m_radioHorizontalLayout;
	QHBoxLayout *m_editHorizontalLayout;
	QVBoxLayout *m_labelVerticalLayout;
	QVBoxLayout *m_editVerticalLayout;

public:
	QPushButton *m_buttonSubmit;

	int m_number1, m_number2;

	enum class ERadioState {
		Sum,
		GCD,
		LCM
	};

	ERadioState m_eRadioState = ERadioState::Sum;

private:
	void initWidgets();
	void makeConnections();
	void setupLayouts();

signals:
	void submitted();
	
public:
	InputDialog(QWidget *parent = nullptr);
	~InputDialog() = default;

	InputDialog(const InputDialog &) = delete;
	InputDialog& operator=(InputDialog &) = delete;
	InputDialog(InputDialog &&) = delete;
	InputDialog& operator=(InputDialog &&) = delete;

	void submit();
};
