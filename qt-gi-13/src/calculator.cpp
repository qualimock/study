#include "calculator.h"

int Calculator::calculateGreatestCommonDivisor(int number1, int number2) {
	while(number1 != number2)
	{
		if(number1 > number2) {
			number1 -= number2;
		}
		else {
			number2 -= number1;
		}
	}

	return number1;
}


int Calculator::calculateLeastCommonMultiplier(int number1, int number2) {
	int count = (number1 < number2) ? number1 : number2;
 
    for (int i = 2; i <= count; i++) {
        if (!((int)number1 % i) && !((int)number2 % i)) {
			return i;
		}
	}

	return 1;
}


int Calculator::calculateSum(int number1, int number2) {
	return number1 + number2;
}
