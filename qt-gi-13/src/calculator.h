#pragma once

class Calculator {
public:
	Calculator() = delete;
	~Calculator() = delete;
	Calculator(const Calculator &) = delete;
	Calculator& operator=(Calculator &) = delete;
	Calculator(Calculator &&) = delete;
	Calculator& operator=(Calculator &&) = delete;

	static int calculateGreatestCommonDivisor(int number1, int number2);
	static int calculateLeastCommonMultiplier(int number1, int number2);
	static int calculateSum(int number1, int number2);
};
