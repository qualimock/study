#include "input_dialog.h"

InputDialog::InputDialog(QWidget *parent)
	: QWidget(parent) {
	this->setWindowTitle("Input");
	this->setMinimumSize(256, 128);

	initWidgets();
	setupLayouts();
	makeConnections();
}


void InputDialog::initWidgets() {
	m_mainVerticalLayout = new QVBoxLayout(this);
	m_radioHorizontalLayout = new QHBoxLayout();
	m_labelVerticalLayout = new QVBoxLayout();
	m_editVerticalLayout = new QVBoxLayout();
	m_editHorizontalLayout = new QHBoxLayout();

	m_labelNumber1 = new QLabel("Number 1");
	m_labelNumber2 = new QLabel("Number 2");

	m_editNumber1 = new QLineEdit();
	m_editNumber2 = new QLineEdit();

	m_radioSum = new QRadioButton("Sum");
	m_radioSum->setChecked(true);

	m_radioGCD = new QRadioButton("GCD");
	m_radioLCM = new QRadioButton("LCM");

	m_buttonSubmit = new QPushButton("Submit");
}


void InputDialog::setupLayouts() {
	m_mainVerticalLayout->addLayout(m_radioHorizontalLayout);
	m_mainVerticalLayout->addLayout(m_editHorizontalLayout);
	m_mainVerticalLayout->addWidget(m_buttonSubmit);

	m_editHorizontalLayout->addLayout(m_editVerticalLayout);
	m_editHorizontalLayout->addLayout(m_labelVerticalLayout);
	
	m_labelVerticalLayout->addWidget(m_labelNumber1);
	m_labelVerticalLayout->addWidget(m_labelNumber2);

	m_editVerticalLayout->addWidget(m_editNumber1);
	m_editVerticalLayout->addWidget(m_editNumber2);

	m_radioHorizontalLayout->addWidget(m_radioSum);
	m_radioHorizontalLayout->addWidget(m_radioGCD);
	m_radioHorizontalLayout->addWidget(m_radioLCM);
}


void InputDialog::makeConnections() {
	connect(m_buttonSubmit, &QPushButton::pressed, this, &InputDialog::submit);
	connect(m_radioSum, &QRadioButton::toggled, this,
			[this] () {
				m_eRadioState = ERadioState::Sum;
			});
	connect(m_radioGCD, &QRadioButton::toggled, this,
			[this] () {
				m_eRadioState = ERadioState::GCD;
			});
	connect(m_radioLCM, &QRadioButton::toggled, this,
			[this] () {
				m_eRadioState = ERadioState::LCM;
			});
}


void InputDialog::submit() {
	if (!(m_editNumber1->text().isEmpty() && m_editNumber2->text().isEmpty())) {
		m_number1 = m_editNumber1->text().toInt();
		m_number2 = m_editNumber2->text().toInt();

		emit submitted();
	}
}
