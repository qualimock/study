#pragma once

#include <QCoreApplication>
#include <QMainWindow>
#include <QWidget>
#include <QVBoxLayout>
#include <QMenuBar>
#include <QAction>
#include <QCloseEvent>
#include <QMessageBox>
#include <QLabel>

#include "input_dialog.h"
#include "calculator.h"

class MainWindow : public QMainWindow {
    Q_OBJECT
	QWidget *m_centralWidget;
	QVBoxLayout *m_layout;

	QMenu *m_mHelp = nullptr;

	QAction *m_aInput = nullptr;
	QAction *m_aCalc = nullptr;
	QAction *m_aHowToUse = nullptr;
	QAction *m_aAbout = nullptr;
	QAction *m_aQuit = nullptr;

	InputDialog *m_inputDialog = nullptr;

public:
	QLabel *m_mainLabel = nullptr;

    MainWindow(QWidget *parent = nullptr);
    ~MainWindow() = default;

	void closeEvent(QCloseEvent *event);
	void showCalculations();

private:
	void initWidgets();
	void setupMenubar();
	void makeConnections();
	void setStyleSheet();
	void showHelp();
	void showAbout();
	void showMessageOnLabel(int number1, int number2,
							InputDialog::ERadioState state,
							bool isResult = false, int resultNumber = 0);
};
