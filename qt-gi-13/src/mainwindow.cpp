#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent) {
	this->setWindowTitle("Волков А.В. ИВТ1-Б22 QT-GI-13");
	this->setFixedSize(380, 120);

	m_centralWidget = new QWidget(this);
	m_layout = new QVBoxLayout();
	m_centralWidget->setLayout(m_layout);
	setCentralWidget(m_centralWidget);

	initWidgets();
	makeConnections();
	setupMenubar();
	setStyleSheet();
}


void MainWindow::initWidgets() {
	m_mHelp = new QMenu("&Help", this);

	m_aInput = new QAction("&Input", this);
	m_aCalc = new QAction("&Calc", this);
	m_aHowToUse = new QAction("How to use", this);
	m_aAbout = new QAction("About", this);
	m_aQuit = new QAction("&Quit", this);

	m_mainLabel = new QLabel("Welcome!");
	m_layout->addWidget(m_mainLabel);

	m_inputDialog = new InputDialog();
}


void MainWindow::setupMenubar() {
	m_mHelp->addAction(m_aHowToUse);
	m_mHelp->addAction(m_aAbout);

	this->menuBar()->addAction(m_aInput);
	this->menuBar()->addAction(m_aCalc);
	this->menuBar()->addMenu(m_mHelp);
	this->menuBar()->addAction(m_aQuit);

	m_aCalc->setDisabled(true);
}


void MainWindow::makeConnections() {
	connect(m_aQuit, &QAction::triggered, this, &QCoreApplication::quit);
	connect(m_aHowToUse, &QAction::triggered, this, &MainWindow::showHelp);
	connect(m_aAbout, &QAction::triggered, this, &MainWindow::showAbout);
	connect(m_aInput, &QAction::triggered, m_inputDialog, &QWidget::show);
	connect(m_inputDialog, &InputDialog::submitted, m_aCalc,
			[this] () {
				m_aCalc->setEnabled(true);
				m_inputDialog->close();
				showMessageOnLabel(m_inputDialog->m_number1,
								   m_inputDialog->m_number2,
								   m_inputDialog->m_eRadioState);
			});
	connect(m_aCalc, &QAction::triggered, this, &MainWindow::showCalculations);
}


void MainWindow::setStyleSheet() {
	m_mainLabel->setStyleSheet(
		"QLabel {"
		"font-family: monospace;"
		"font-size: 15px;"
		"}");
}


void MainWindow::showHelp() {}


void MainWindow::showAbout() {}


void MainWindow::showMessageOnLabel(int number1, int number2,
									InputDialog::ERadioState state,
									bool isResult, int resultNumber) {
	QString labelString;
	switch (state) {
	case InputDialog::ERadioState::Sum:
		labelString += "Sum";
		break;
	case InputDialog::ERadioState::GCD:
		labelString += "GCD";
		break;
	case InputDialog::ERadioState::LCM:
		labelString += "LCM";
		break;
	}

	labelString += "(" + QString::number(number1) + ", " + QString::number(number2) + ")";

	if (isResult) {
		labelString += " = " + QString::number(resultNumber);
	}

	m_mainLabel->setText(labelString);
}


void MainWindow::closeEvent(QCloseEvent *event) {
	QMessageBox::StandardButton submitButton =
		QMessageBox::question(this, "Quit",
							  tr("Are you sure?\n"),
							  QMessageBox::Yes | QMessageBox::No,
							  QMessageBox::Yes);

	if (submitButton != QMessageBox::Yes) {
        event->ignore();
    } else {
        event->accept();
		m_inputDialog->close();
    }
}


void MainWindow::showCalculations() {
	int number1 = m_inputDialog->m_number1;
	int number2 = m_inputDialog->m_number2;
	int result = 0;

	switch (m_inputDialog->m_eRadioState) {
	case InputDialog::ERadioState::Sum:
		qDebug() << "Sum(" << number1 << ", " << number2 << ")";
		result = Calculator::calculateSum(number1, number2);
		break;

	case InputDialog::ERadioState::GCD:
		qDebug() << "GCD(" << number1 << ", " << number2 << ")";
		result = Calculator::calculateGreatestCommonDivisor(number1, number2);
		break;

	case InputDialog::ERadioState::LCM:
		qDebug() << "LCM(" << number1 << ", " << number2 << ")";
		result = Calculator::calculateLeastCommonMultiplier(number1, number2);
		break;
	}

	showMessageOnLabel(number1, number2, m_inputDialog->m_eRadioState, true, result);
}
