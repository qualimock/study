#include <iostream>

// #1, #2

// Rocket

class Rocket
{
	double m_velocity;

public:
	Rocket(unsigned fuel);
	virtual void fly();
	
protected:
	unsigned m_solidFuel;
};

Rocket::Rocket(unsigned fuel)
	: m_solidFuel(fuel)
{}

void Rocket::fly()
{
	m_velocity = m_solidFuel;
	std::cout << "WEEEE" << std::endl;
}

// Airplane

class Airplane
{
	double m_velocity;

public:
	Airplane(unsigned fuel);
	virtual void fly();

protected:
	unsigned m_liquidFuel;
};

Airplane::Airplane(unsigned fuel)
	: m_liquidFuel(fuel)
{}

void Airplane::fly()
{
	m_velocity = m_liquidFuel * 1.5;
	std::cout << "WEEEEEEEEEEEEEE" << std::endl;
}

class Jetplane : public Rocket, public Airplane
{
	double m_velocity;

public:
	Jetplane(unsigned fuel);
	void fly() override;
};

// Jetplane

Jetplane::Jetplane(unsigned fuel)
	: Rocket((unsigned) fuel / 2)
	, Airplane((unsigned) fuel / 2)
{}

void Jetplane::fly()
{
	m_velocity = m_solidFuel + 2*m_liquidFuel;
	std::cout << "WEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE" << std::endl;
}

class _747 : public Jetplane
{
	double m_velocity;
	unsigned m_seatNumber;

public:
	_747(unsigned fuel);
	void fly() override;
};

// 747

_747::_747(unsigned fuel)
	: Jetplane(fuel)
{}

void _747::fly()
{
	m_velocity = m_solidFuel + 2*m_liquidFuel - m_seatNumber;
	std::cout << "WEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE" << std::endl;
}

// #3

class Vehicle
{
public:
	virtual void go() = 0;
	virtual void refuel(unsigned fuel) = 0;

protected:
	unsigned m_fuel;
	unsigned m_velocity;
};

class Car : public Vehicle
{	
public:
	Car() = default;
	void go() override;
	void refuel(unsigned fuel) override;
};

void Car::refuel(unsigned fuel)
{
	std::cout << "FSHHHHH" << std::endl;
	m_fuel = fuel;
}

void Car::go()
{
	m_velocity = m_fuel * 10;
	std::cout << "VZHHHHHHHH" << std::endl;
}

class Bus : public Vehicle
{
	unsigned m_seats;

public:
	Bus(unsigned seats);
	void go() override;
	void refuel(unsigned fuel) override;
};

Bus::Bus(unsigned seats)
	: m_seats(seats)
{}

void Bus::refuel(unsigned int fuel)
{
	std::cout << "FSHHHHHHHHHHHHHH" << std::endl;
	m_fuel = fuel;
}

void Bus::go()
{
	m_velocity = m_fuel;
	std::cout << "VZH" << std::endl;
}

int main(int argc, char **argv)
{
	Rocket rocket(100);
	Airplane airplane(100);
	Jetplane jetplane(100);
	_747 semsotsoroksem(100);

	rocket.fly();
	airplane.fly();
	jetplane.fly();
	semsotsoroksem.fly();

	Car car;
	Bus bus(10);

	car.refuel(10);
	bus.refuel(10);

	car.go();
	bus.go();
	
	return 0;
}
