#include <iostream>

#include "String/string.h"
#include "Complex/complex.h"
#include "Decimal/decimal.h"

int main(int argc, char **argv)
{	
	String str("Hello");

	std::cout << str << std::endl;

	str = "hi";

	str.append("aaa");

	std::cout << str << std::endl;

	Complex complex(10, 20);

	std::cout << complex << std::endl;

	Decimal num1(100);
	Decimal num2(100);

	if (num1 > num2)
		std::cout << "More" << std::endl;
	else if (num1 < num2)
		std::cout << "Less" << std::endl;
	else
		std::cout << "Equal" << std::endl;

	complex[2] = 'e';

	std::cout << complex << std::endl;
	
	return 0;
}
