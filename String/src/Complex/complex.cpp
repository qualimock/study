#include "complex.h"

#include <string>

Complex::Complex()
{
	m_real = 0;
	m_imaginary = 0;
}

Complex::Complex(const int &real, const int &imaginary)
{
	this->append(std::to_string(real).c_str());
	this->append('i');
	this->append('i');
	this->append(std::to_string(imaginary).c_str());
	m_real = real;
	m_imaginary = imaginary;	
}

Complex operator+(Complex left, const Complex &right)
{
	left.m_real += right.m_real;
	left.m_imaginary += right.m_imaginary;
	return left;
}

// Complex operator*(Complex left, const Complex &right)
// {
	
// }

// Complex operator==(const Complex &left, const Complex &right)
// {
	
// }
