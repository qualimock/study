#pragma once

#include "../String/string.h"

class Complex : public String
{
	int m_real;
	int m_imaginary;

	void checkReliability();

public:
	Complex();
	Complex(const int &real, const int &imaginary);
	
	friend Complex operator+(Complex left, const Complex &right);
	friend Complex operator*(Complex left, const Complex &right);
	friend Complex operator==(const Complex &left, const Complex &right);

	int real() { return m_real; }
	int imaginary() { return m_imaginary; }
};
