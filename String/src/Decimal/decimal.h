#pragma once

#include "../String/string.h"

class Decimal : public String
{
	int m_number;

public:
	Decimal();
	Decimal(const int number);

	bool operator>(const Decimal &number);
	bool operator<(const Decimal &number);
	bool operator==(const Decimal &number);
};
