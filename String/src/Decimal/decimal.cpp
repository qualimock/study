#include "decimal.h"

Decimal::Decimal()
	: m_number(0)
{
	this->append(std::to_string(m_number).c_str());
}

Decimal::Decimal(const int number)
	: m_number(number)
{
	this->append(std::to_string(m_number).c_str());
}
bool Decimal::operator>(const Decimal &number)
{
	if (m_number > number.m_number)
		return true;
	return false;
}

bool Decimal::operator<(const Decimal &number)
{
	if (m_number < number.m_number)
		return true;
	return false;
}

bool Decimal::operator==(const Decimal &number)
{
	if (m_number == number.m_number)
		return true;
	return false;
}
