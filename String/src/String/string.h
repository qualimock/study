#pragma once

#include <ostream>

class String
{
	char *m_pChar;
	int m_length;

public:
	String();
	String(const char *str);
	String(const char chr);
	~String();

	void operator=(const char *str);
	char & operator[](unsigned position);
	friend std::ostream& operator<<(std::ostream &os, const String &str);

	String append(const char *str);
	String append(const char chr);
	
	int len() { return m_length; }
	void clear();
};

unsigned strlen(char *str);
