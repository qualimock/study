#include "string.h"

String::String()
	: m_pChar(0)
	, m_length(0)
{}

String::String(const char *str)
	: m_pChar(const_cast<char *>(str))
	, m_length(strlen(const_cast<char *>(str)))
{}

String::String(const char chr)
	: m_length(1)
{
	*m_pChar = chr + 0;
}

void String::operator=(const char *str)
{
	m_pChar = const_cast<char *>(str);
	m_length = strlen(const_cast<char *>(str));
}

String::~String()
{
	m_pChar = nullptr;
}

void String::clear()
{
	m_pChar = nullptr;
	m_length = 0;
}

std::ostream& operator<<(std::ostream &os, const String &str)
{
	os << str.m_pChar;
	return os;
}

char & String::operator[](unsigned position)
{
	return m_pChar[position];
}

String String::append(const char *str)
{
	unsigned strLength = strlen(const_cast<char *>(str));
	unsigned newLength = m_length + strLength;

	char *newString = new char[newLength + 1];
	for (int i = 0; i < m_length; i++)
	{
		newString[i] = m_pChar[i];
	}

	for (int i = 0; i < strLength; i++)
	{
		newString[m_length + i] = str[i];
	}

	newString[newLength] = 0;
	m_pChar = newString;

	return *this;
}

String String::append(const char chr)
{
	while (m_pChar[m_length] != 0)
	{
		m_length++;
	}

	char *newString = new char[m_length + 2];

	for (int i = 0; i < m_length; i++)
	{
		newString[i] = m_pChar[i];
	}

	newString[m_length] = chr;
	m_pChar = newString;

	return *this;
}

unsigned strlen(char *str)
{
	int count = 0;
	while (str[count] != 0)
	{
		count++;
	}
	return count;
}
