cmake_minimum_required(VERSION 3.16 FATAL_ERROR)
project(Day12)

set(CMAKE_CXX_STANDARD 17)

add_executable(${PROJECT_NAME}
  src/main.cpp)

set_target_properties(${PROJECT_NAME} PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

