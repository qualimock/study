#include <iostream>

int main(int argc, char **argv)
{
	// #1
	const int FIELD_SIZE = 5;
	char ttt[FIELD_SIZE][FIELD_SIZE];

	// #2
	for (int i = 0; i < FIELD_SIZE; i++)
	{
		for (int j = 0; j < FIELD_SIZE; j++)
		{
			ttt[i][j] = '_';
		}
	}

	// OUTPUT
	for (int i = 0; i < FIELD_SIZE; i++)
	{
		for (int j = 0; j < FIELD_SIZE; j++)
		{
			std::cout << ttt[i][j] << " ";
		}
		std::cout << std::endl;
	}
	
	return 0;
}
