#pragma once


template<class T>
class List {
public:
    List();
    ~List() = default;

	void insert(T value);
	void append(T value);

	int is_present(T value) const;
	int is_empty() const { return head == 0; };
	int count() const { return theCount; };

private:
	class ListCell {
	public:
		ListCell(T value, ListCell *cell)
			: val(value)
			, next(cell) {}

		T val;
		ListCell *next;
	};

	ListCell *head;
	ListCell *tail;
	int theCount;
};
