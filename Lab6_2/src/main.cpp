#include <iostream>
#include <vector>
#include <algorithm>


void split_string(const std::string &str,
				  const std::string &delimiter,
				  std::vector<std::string> &result) {
	size_t pos;

	if ((pos = str.find(delimiter)) && static_cast<int>(pos) == -1) {
		result.emplace_back(str);
		return;
	}

	result.emplace_back(str.substr(0, pos));

	split_string(str.substr(pos+1, str.size()), delimiter, result);
}


std::string trim(const std::string& str,
                 const std::string& whitespace = " \t")
{
    const auto strBegin = str.find_first_not_of(whitespace);
    if (strBegin == std::string::npos)
        return ""; // no content

    const auto strEnd = str.find_last_not_of(whitespace);
    const auto strRange = strEnd - strBegin + 1;

    return str.substr(strBegin, strRange);
}

std::string reduce(const std::string& str,
                   const std::string& fill = " ",
                   const std::string& whitespace = " \t")
{
    // trim first
    auto result = trim(str, whitespace);

    // replace sub ranges
    auto beginSpace = result.find_first_of(whitespace);
    while (beginSpace != std::string::npos)
    {
        const auto endSpace = result.find_first_not_of(whitespace, beginSpace);
        const auto range = endSpace - beginSpace;

        result.replace(beginSpace, range, fill);

        const auto newStart = beginSpace + fill.length();
        beginSpace = result.find_first_of(whitespace, newStart);
    }

    return result;
}


void vector_input(std::vector<int> &vec) {
	std::cout << "Enter vector elements. Size = " << vec.size() << std::endl;

	std::string input_numbers;
	std::vector<std::string> numbers;
	std::getline(std::cin, input_numbers);
	split_string(reduce(input_numbers), " ", numbers);

	for (int i = 0; i < static_cast<int>(vec.size()); i++) {
		try {
			vec[i] = std::stoi(numbers[i]);
		} catch (std::exception &e) {
			std::cout << "Error: Incorrect input syntax. Aborting" << std::endl;
			exit(-1);
		}
	}
}


void print_vector(std::vector<int> &vec) {
	for (int value : vec) {
		std::cout << value << " ";
	}
	std::cout << std::endl;
}


int find_closest(std::vector<int> &sorted_vec, int num) {
	std::vector<int>::iterator pivot = std::next(sorted_vec.begin(), sorted_vec.size() / 2);

	int left = *std::lower_bound(sorted_vec.begin(), pivot, num);
	int right = *std::lower_bound(pivot, sorted_vec.end(), num);

	std::cout << left << " " << right << std::endl;

	if ((num - right) < (num - left)) {
		if (right < left) {
			return right;
        }
	}
	return left;
}


int main(int argc, char **argv)
{
	// int n, k;
	// {
	// 	std::string tmp;

	// 	std::cout << "Enter n: ";
	// 	std::getline(std::cin, tmp);
	// 	n = std::stoi(tmp);

	// 	std::cout << "Enter k: ";
	// 	std::getline(std::cin, tmp);
	// 	k = std::stoi(tmp);
	// }


    // temp
	int n = 5, k = 5;

	std::vector<int> array_n(n), array_k(k);
    // vector_input(array_n); vector_input(array_k);

	// temp
	array_n = {5, 3, 1, 7, 9};
	array_k = {1, 4, 6, 2, 8};
	
	std::sort(array_n.begin(), array_n.end());

	print_vector(array_n); print_vector(array_k);

	for (int num_k : array_k) {
		int num = find_closest(array_n, num_k);
		std::cout << num_k << ": " << num << std::endl;
	}

	return 0;
}
