#include <iostream>


template <typename T>
class List
{
    typedef struct Node
    {
        T    data{};
        Node *next{};
        Node *back{};
    } Node;

    Node *head{};

public:
    List() noexcept;
    ~List() noexcept;

    void add_element(const T &data) noexcept;
    void pop_element(const unsigned &elem);
    [[nodiscard]] Node* return_head() const noexcept;
};


class String
{
    List<char> str;

public:
    String() noexcept = default;
    explicit String(const char *str) noexcept;
    ~String() noexcept = default;

    [[nodiscard]] char* data() const noexcept;
};


template <typename T>
List<T>::List() noexcept { head = nullptr; }


template <typename T>
List<T>::~List() noexcept {
    while (head != nullptr)
    {
        Node *oldHead = head;
        head = head->next;

        delete oldHead;
    }
}


template <typename T>
void List<T>::add_element(const T &data) noexcept {
    Node *node = new Node;
    node->data = data, node->next = nullptr;

    if (head == nullptr)
    {
        node->back = nullptr, head = node;
    }
    else
    {
        Node *currentNode = head;

        while (currentNode->next != nullptr)
        {
            Node *oldNode = currentNode;
            currentNode = currentNode->next, currentNode->back = oldNode;
        }

        currentNode->next = node, node->back = currentNode;
    }
}

template <typename T>
void List<T>::pop_element(const unsigned &elem) {
    Node *currentNode = head;

    for (unsigned i = 0; i <= elem; i++)
    {
        if (currentNode == nullptr)
        {
            throw std::range_error("Error! The element is out of bounds!");
        }
        else if (elem == 0)
        {
            Node *oldHead = head;
            head = currentNode->next, head->back = nullptr;

            delete oldHead;
        }
        else if (currentNode->next == nullptr)
        {
            Node *oldBack = currentNode->back, *oldNode = currentNode;
            oldBack->next = nullptr;

            delete oldNode;
        }
        else if (i == elem)
        {
            Node *oldBack = currentNode->back, *oldHead = currentNode;
            currentNode = currentNode->next, oldBack->next = currentNode, currentNode->back = oldBack;

            delete oldHead;
        }
        else
        {
            currentNode = currentNode->next;
        }
    }
}

template<typename T>
typename List<T>::Node* List<T>::return_head() const noexcept { return head; }


String::String(const char *str) noexcept {
    unsigned counter = 0;

    while (str[counter] != '\0')
    {
        this->str.add_element(str[counter++]);
    }
}


char* String::data() const noexcept {
    auto head = str.return_head();
    unsigned size = 0;

    while (head->next != nullptr)
    {
        size++, head = head->next;
    }

    char *array = new char[size];
    head = str.return_head(), size = 0;

    while (head != nullptr)
    {
        array[size++] = head->data, head = head->next;
    }

    return array;
}


int main()
{
    String hello("hello");

    std::cout << hello.data() << std::endl;

    return 0;
}
