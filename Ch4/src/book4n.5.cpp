#include <iostream>

struct StudentCollection {
    std::string name;
    std::string group;
    unsigned    yo{};
};


template <typename T>
class List
{
    typedef struct Node
    {
        T    data;
        Node *next{};
        Node *back{};
    } Node;

    Node *head{};

public:
    List() noexcept;
    ~List() noexcept;

    void add_element(const T &data) noexcept;

    void pop_element(const unsigned &elem);

    [[nodiscard]] Node* return_head() const noexcept;
};


template <typename T>
List<T>::List() noexcept { head = nullptr; }


template <typename T>
List<T>::~List() noexcept
{
    while (head != nullptr)
    {
        Node *oldHead = head;
        head = head->next;

        delete oldHead;
    }
}


template <typename T>
void List<T>::add_element(const T &data) noexcept
{
    Node *node = new Node;
    node->data = data, node->next = nullptr;

    if (head == nullptr)
    {
        node->back = nullptr, head = node;
    }
    else
    {
        Node *currentNode = head;

        while (currentNode->next != nullptr)
        {
            Node *oldNode = currentNode;
            currentNode = currentNode->next, currentNode->back = oldNode;
        }

        currentNode->next = node, node->back = currentNode;
    }
}


template <typename T>
void List<T>::pop_element(const unsigned &elem) {
    Node *currentNode = head;

    for (unsigned i = 0; i <= elem; i++)
    {
        if (currentNode == nullptr)
        {
            throw std::range_error("Error! The element is out of bounds!");
        }
        else if (elem == 0)
        {
            Node *oldHead = head;
            head = currentNode->next, head->back = nullptr;

            delete oldHead;
        }
        else if (currentNode->next == nullptr)
        {
            Node *oldBack = currentNode->back, *oldNode = currentNode;
            oldBack->next = nullptr;

            delete oldNode;
        }
        else if (i == elem)
        {
            Node *oldBack = currentNode->back, *oldHead = currentNode;
            currentNode = currentNode->next, oldBack->next = currentNode, currentNode->back = oldBack;

            delete oldHead;
        }
        else
        {
            currentNode = currentNode->next;
        }
    }
}


template<typename T>
typename List<T>::Node* List<T>::return_head() const noexcept { return head; }


int main() {
    List<StudentCollection> list;

    while (true)
    {
        StudentCollection student;

        std::cout << "Enter the name of student (for exit enter 0): ";
        std::cin >> student.name;

        if (student.name == "0")
            break;

        std::cout << "Enter the group of student: ";
        std::cin >> student.group;

        std::cout << "Enter the student age: ";
        std::cin >> student.yo;

        list.add_element(student);

        auto head = list.return_head();
        unsigned counter = 0;

        while (head != nullptr)
        {
            std::cout << '[' << ++counter << "] Name: " << head->data.name << " | Group: " << head->data.group <<
            " | Years old: " << head->data.yo << std::endl;

            head = head->next;
        }
    }

    while (true)
    {
        auto head = list.return_head();
        unsigned counter = 0;

        while (head != nullptr)
        {
            std::cout << '[' << ++counter << "] Name: " << head->data.name << " | Group: " << head->data.group <<
            " | Years old: " << head->data.yo << std::endl;

            head = head->next;
        }

        int number;

        std::cout << "What student you want to remove from list? Enter the number (for exit enter -1): ";
        std::cin >> number;

        if (number == -1)
            break;

        list.pop_element(number - 1);
    }

    return 0;
}
