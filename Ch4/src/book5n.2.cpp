#include <iostream>


class Vehicle {
private:
    std::string manufacturersName;
    std::string model;
    unsigned    dateOfManufacture{};
public:
    Vehicle() noexcept = default;

    explicit Vehicle(const std::string &manufacturersName,
					 const std::string &model,
					 const unsigned &dateOfManufacture) noexcept;

    ~Vehicle() noexcept = default;

    void set_manufacturers_name(const std::string &_manufacturersName) noexcept;
    void set_vehicle_model(const std::string &_model) noexcept;
    void set_date_of_manufacture(const unsigned &_dateOfManufacture) noexcept;

    [[nodiscard]] std::string get_manufacturers_name() const noexcept;
    [[nodiscard]] std::string get_vehicle_model() const noexcept;
    [[nodiscard]] unsigned get_date_of_manufacture() const noexcept;

    void print_data() const noexcept;
};


Vehicle::Vehicle(const std::string &manufacturersName,
				 const std::string &model,
                 const unsigned &dateOfManufacture) noexcept
{
    this->manufacturersName = manufacturersName, this->model = model, this->dateOfManufacture = dateOfManufacture;
}


std::string Vehicle::get_manufacturers_name() const noexcept { return manufacturersName; }


void Vehicle::set_manufacturers_name(const std::string &_manufacturersName) noexcept
{
    manufacturersName = _manufacturersName;
}


std::string Vehicle::get_vehicle_model() const noexcept { return model; }


void Vehicle::set_vehicle_model(const std::string &_model) noexcept { model = _model; }


unsigned Vehicle::get_date_of_manufacture() const noexcept { return dateOfManufacture; }


void Vehicle::set_date_of_manufacture(const unsigned &_dateOfManufacture) noexcept
{
    dateOfManufacture = _dateOfManufacture;
}


void Vehicle::print_data() const noexcept
{
    std::cout << dateOfManufacture << ' ' << manufacturersName << ' ' << model << std::endl;
}


int main() {
    Vehicle vehicle("Chevrolet", "Impala", 1957);

    vehicle.print_data();

    return 0;
}
