#include <iostream>

class String {
    char *array;

public:
    String() noexcept;
    explicit String(const char *str) noexcept;
    ~String() noexcept { delete[] array; };

    String& operator=(const char *str);

    [[nodiscard]] char* data() const noexcept;
    void output() const noexcept;
};


String::String() noexcept {
    array = new char[1];
    array[0] = 0;
}


String::String(const char *str) noexcept {
    unsigned size = 0;

    while (str[size++] != '\0');

    array = new char[++size];
    array[0] = (char)size;

    for (unsigned i = 1, k = 0; i < size - 1; i++, k++)
    {
        array[i] = str[k];
    }
}

String& String::operator=(const char *str) {
    delete[] array;

    unsigned size = 0;

    while (str[size++] != '\0');

    array = new char[++size];
    array[0] = (char)size;

    for (unsigned i = 1, k = 0; i < size - 1; i++, k++)
    {
        array[i] = str[k];
    }

    return *this;
}


char* String::data() const noexcept {
    char *copyArray = new char[array[0]];

    for (unsigned i = 1, k = 0; i < array[0]; i++, k++)
    {
        copyArray[k] = array[i];
    }

    return copyArray;
}


void String::output() const noexcept {
    for (unsigned i = 1; i < array[0] - 1; i++)
    {
        printf("%c", array[i]);
    }

    printf("\n");
}


int main() {
    String fullStr("This is full string"), copyStr("It's copy string");
    copyStr = fullStr.data();

    copyStr.output();

    return 0;
}
