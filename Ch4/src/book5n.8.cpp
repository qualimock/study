#include <iostream>


enum HasTheRightAmountOfPoints {
    Yes,
    No,
};


class StudentCollection {
private:
    std::string name;
    std::string group;
    unsigned    yo{};
    unsigned    record{};
public:
    StudentCollection() noexcept = default;

    ~StudentCollection() noexcept = default;

    StudentCollection& operator=(const StudentCollection &object) noexcept;

    [[nodiscard]] std::string get_name() const noexcept;

    void set_name(const std::string &_name) noexcept;

    [[nodiscard]] std::string get_group() const noexcept;

    void set_group(const std::string &_group) noexcept;

    [[nodiscard]] unsigned get_years_old() const noexcept;

    void set_years_old(const unsigned &_yo) noexcept;

    [[nodiscard]] unsigned get_record() const noexcept;

    void set_record(const unsigned &_record);

    [[nodiscard]] HasTheRightAmountOfPoints records_within_range(const unsigned &start, const unsigned &end) const
    noexcept;
};


template <typename T>
class List {
private:
    typedef struct Node {
        T    data;
        Node *next{};
        Node *back{};
    } Node;

    Node *head{};
public:
    List() noexcept;

    ~List() noexcept;

    void add_element(const T &data) noexcept;

    void pop_element(const unsigned &elem);

    [[nodiscard]] Node* return_head() const noexcept;
};


int main() {
    List<StudentCollection> list;

    while (true) {
        StudentCollection student;
        std::string userData;
        unsigned uUserData;

        std::cout << "Enter the name of student (for exit enter 0): ";
        std::cin >> userData;

        if (userData == "0")
            break;

        student.set_name(userData);

        std::cout << "Enter the group of student: ";
        std::cin >> userData;

        student.set_group(userData);

        std::cout << "Enter the student`s age: ";
        std::cin >> uUserData;

        student.set_years_old(uUserData);

        std::cout << "Enter the record of student: ";
        std::cin >> uUserData;

        student.set_record(uUserData);

        list.add_element(student);

        auto head = list.return_head();
        unsigned counter = 0;

        while (head != nullptr) {
            std::cout << '[' << ++counter << "] Name: " << head->data.get_name() << " | Group: " <<
                      head->data.get_group() << " | Years old: " << head->data.get_years_old() << " | Record: " <<
                      head->data.get_record() << std::endl;

            head = head->next;
        }
    }

    while (true) {
        auto head = list.return_head();
        unsigned counter = 0;

        while (head != nullptr) {
            std::cout << '[' << ++counter << "] Name: " << head->data.get_name() << " | Group: " <<
                      head->data.get_group() <<" | Years old: " << head->data.get_years_old() << " | Record: " <<
                      head->data.get_record() <<  std::endl;

            head = head->next;
        }

        int number;

        std::cout << "What student you want to remove from list? Enter the number (for exit enter -1): ";
        std::cin >> number;

        if (number == -1)
            break;

        list.pop_element(number - 1);
    }

    auto head = list.return_head();
    unsigned counter = 0, start, end;

    std::cout <<
              "With what score do you need students? Enter the start and end separated by a space, respectively: ";
    std::cin >> start >> end;

    while (head != nullptr) {
        if (head->data.records_within_range(start, end) == HasTheRightAmountOfPoints::Yes)
            std::cout << '[' << ++counter << "] Name: " << head->data.get_name() << " | Group: " <<
                      head->data.get_group() <<" | Years old: " << head->data.get_years_old() << " | Record: " <<
                      head->data.get_record() << std::endl;

        head = head->next;
    }

    return 0;
}


StudentCollection& StudentCollection::operator=(const StudentCollection &object) noexcept {
    name = object.name, group = object.group, yo = object.yo, record = object.record;

    return *this;
}


std::string StudentCollection::get_name() const noexcept { return name; }


void StudentCollection::set_name(const std::string &_name) noexcept { name = _name; }


std::string StudentCollection::get_group() const noexcept { return group; }


void StudentCollection::set_group(const std::string &_group) noexcept { group = _group; }


unsigned StudentCollection::get_years_old() const noexcept { return yo; }


void StudentCollection::set_years_old(const unsigned &_yo) noexcept { yo = _yo; }


unsigned StudentCollection::get_record() const noexcept { return record; }


HasTheRightAmountOfPoints StudentCollection::records_within_range(const unsigned &start, const unsigned &end) const
noexcept {
    if (record >= start && record <= end)
        return HasTheRightAmountOfPoints::Yes;

    return HasTheRightAmountOfPoints::No;
}


void StudentCollection::set_record(const unsigned &_record)  {
    if (_record > 100)
        throw std::logic_error("Maximum evaluation score 100");

    record = _record;
}


template <typename T>
List<T>::List() noexcept { head = nullptr; }


template <typename T>
List<T>::~List() noexcept {
    while (head != nullptr) {
        Node *oldHead = head;
        head = head->next;

        delete oldHead;
    }
}


template <typename T>
void List<T>::add_element(const T &data) noexcept {
    Node *node = new Node;

    node->data = data, node->next = nullptr;

    if (head == nullptr)
        node->back = nullptr, head = node;
    else {
        Node *currentNode = head;

        while (currentNode->next != nullptr) {
            Node *oldNode = currentNode;

            currentNode = currentNode->next, currentNode->back = oldNode;
        }

        currentNode->next = node, node->back = currentNode;
    }
}


template <typename T>
void List<T>::pop_element(const unsigned &elem) {
    Node *currentNode = head;

    for (unsigned i = 0; i <= elem; i++) {
        if (currentNode == nullptr)
            throw std::range_error("Error! The element is out of bounds!");
        else if (elem == 0) {
            Node *oldHead = head;
            head = currentNode->next, head->back = nullptr;

            delete oldHead;
        } else if (currentNode->next == nullptr) {
            Node *oldBack = currentNode->back, *oldNode = currentNode;
            oldBack->next = nullptr;

            delete oldNode;
        } else if (i == elem) {
            Node *oldBack = currentNode->back, *oldHead = currentNode;
            currentNode = currentNode->next, oldBack->next = currentNode, currentNode->back = oldBack;

            delete oldHead;
        } else
            currentNode = currentNode->next;
    }
}


template<typename T>
typename List<T>::Node* List<T>::return_head() const noexcept { return head; }
