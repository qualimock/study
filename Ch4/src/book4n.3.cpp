#include <iostream>
#include <cstring>


class String {
    char *array;
    unsigned size = 0;

public:
    String() noexcept;
    explicit String(const char *str) noexcept;
    ~String() noexcept;

    String& operator=(const char *str);

    [[nodiscard]] char* data() const noexcept;
    static char* substring(const String &object, const unsigned &start, const unsigned &len);
    static void replace_string(String &source, const String &target, const String &replaceText);
};


String::String() noexcept { array = nullptr; }


String::String(const char *str) noexcept {
    while (str[size++] != '\0');

    array = new char[size];

    for (unsigned i = 0; i < size - 1; i++)
    {
        array[i] = str[i];
    }
}


String::~String() noexcept { delete[] array; }


String& String::operator=(const char *str) {
    delete[] array, size = 0;

    while (str[size++] != '\0');

    array = new char[size];

    for (unsigned i = 0; i < size - 1; i++)
    {
        array[i] = str[i];
    }

    return *this;
}


char* String::data() const noexcept { return array; }


char* String::substring(const String &object, const unsigned &start, const unsigned &len) {
    if (start + len > object.size)
    {
        throw std::range_error("Error! Substring out of line!");
    }

    char *substr = new char[len + 1];

    for (unsigned i = start, k = 0; i < start + len; i++, k++)
    {
        substr[k] = object.array[i];
    }

    return substr;
}


void String::replace_string(String &source, const String &target, const String &replaceText) {
    if (target.size > source.size)
    {
        throw std::range_error("Error! Size of target string > original string");
    }

    for (unsigned i = 0; i < source.size - target.size; i++)
    {
        if (!strcmp(substring(source, i, target.size - 1), target.array))
        {
            char *copyArray = new char[source.size];

            for (unsigned k = 0; k < source.size; k++)
            {
                copyArray[k] = source.array[k];
            }

            delete[] source.array, source.size += replaceText.size;
            source.array = new char[source.size];

            for (unsigned k = 0, m = 0; k < source.size; k++, m++)
            {
                if (k != i)
                {
                    source.array[k] = copyArray[m];
                }
                else
                {
                    unsigned j = k;

                    for (unsigned l = 0; j < k + replaceText.size; j++, l++)
                    {
                        source.array[j] = replaceText.array[l];
                    }

                    k = j - 2, m += target.size - 2;
                }
            }
        }
    }
}


int main()
{
    String fullStr("helloxyzhelloxyz"), xyz("xyz"), ffff("ffff");

    String::replace_string(fullStr, xyz, ffff);

    std::cout << fullStr.data() << std::endl;

    return 0;
}
