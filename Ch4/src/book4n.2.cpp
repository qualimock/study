#include <iostream>

class String {
    char *array;
    unsigned size = 0;

public:
    String() noexcept;
    explicit String(const char *str) noexcept;
    ~String() noexcept;

    String& operator=(const char *str);

    static char* substring(const String &object, const unsigned &start, const unsigned &len);
};


String::String() noexcept { array = nullptr; }


String::String(const char *str) noexcept {
    while (str[size++] != '\0');

    array = new char[size];

    for (unsigned i = 0; i < size - 1; i++)
    {
        array[i] = str[i];
    }
}


String::~String() noexcept { delete[] array; }


String& String::operator=(const char *str) {
    delete[] array, size = 0;

    while (str[size++] != '\0');

    array = new char[size];

    for (unsigned i = 0; i < size - 1; i++)
    {
        array[i] = str[i];
    }

    return *this;
}


char* String::substring(const String &object, const unsigned &start, const unsigned &len) {
    if (start + len > object.size)
    {
        throw std::range_error("Error! Substring out of line!");
    }

    char *substr = new char[len + 1];

    for (unsigned i = start, k = 0; i < start + len; i++, k++)
    {
        substr[k] = object.array[i];
    }

    return substr;
}


int main()
{
    String fullStr("hello");

    std::cout << String::substring(fullStr, 2, 1)
			  << String::substring(fullStr, 4, 1)
			  << String::substring(fullStr, 2, 1) << std::endl;

    return 0;
}
