#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>


void split_string(const std::string &str,
				  const std::string &delimiter,
				  std::vector<std::string> &result) {
	size_t pos;

	if ((pos = str.find(delimiter)) && static_cast<int>(pos) == -1) {
		result.emplace_back(str);
		return;
	}

	result.emplace_back(str.substr(0, pos));

	split_string(str.substr(pos+1, str.size()), delimiter, result);
}


int main(int argc, char **argv) {
	int max_frequently_words_amount;
	std::string input_text;
	std::map<std::string, int> counted_words;

	std::cout << "Enter 'k': ";
	std::getline(std::cin, input_text);
	max_frequently_words_amount = std::stoi(input_text);

	std::cout << "Enter text: ";
	std::getline(std::cin, input_text);

	{
		std::vector<std::string> words;
		split_string(input_text, "\n", words);

		std::vector<std::string> tmp;
		for (const auto &word : words) {
			split_string(word, " ", tmp);
		}

		words = std::move(tmp);

        for (const auto &word : words) {
			counted_words[word]++;
        }
	}

	std::vector<std::pair<std::string, int>> word_pairs;
	for (const auto &pair : counted_words) {
		word_pairs.emplace_back(pair);
	}

	std::sort(word_pairs.begin(), word_pairs.end(),
			  [&](const auto &p1, const auto &p2){
				  return p1.second > p2.second;
			  });

	if (static_cast<int>(word_pairs.size()) < max_frequently_words_amount) {
		max_frequently_words_amount = word_pairs.size();
	}

	for (int i = 0; i < max_frequently_words_amount; i++) {
		std::cout << "'" << word_pairs[i].first << "': " << word_pairs[i].second << std::endl; 
	}
	
	return 0;
}
