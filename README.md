# Study

Those are my university programming labs

## New project

You can initialize a new project with:

```bash
$ ./init_project.sh <project name>
```

## Build and run

Each directory is a project which builds with:

```bash
$ make
```

To run:

```bash
$ make run
```

To debug with gdb:

```bash
$ make debug
```
