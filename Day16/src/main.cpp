#include <iostream>
#include <string>

int main(int argc, char **argv)
{
	if (argc > 1)
	{
		std::cerr << "There shouldn't be any arguments" << std::endl;
	}

	std::string str;

	std::cin >> str;

	std::clog << "Entered str\nOutput:" << std::endl;

	std::cout << str << std::endl;
	
	return 0;
}
