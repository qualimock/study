#pragma once

#include "../Triangle/triangle.h"

class Isosceles : public Triangle
{
public:
	Isosceles();
	Isosceles(double ribs, double base);
};
