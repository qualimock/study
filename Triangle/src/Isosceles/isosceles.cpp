#include "isosceles.h"

Isosceles::Isosceles()
	: Triangle()
{}

Isosceles::Isosceles(double ribs, double base)
	: Triangle(ribs, ribs, base)
{}
