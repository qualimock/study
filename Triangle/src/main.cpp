#include <iostream>

#include "Triangle/triangle.h"
#include "Isosceles/isosceles.h"

void infoOutput(const Triangle &tri)
{
	std::cout << "Triangle:\n\tSides: " << tri.side(0) << " " << tri.side(1) << " " << tri.side(2) << std::endl;
	std::cout << "\tAngles: " << tri.angle(0) << " " << tri.angle(1) << " " << tri.angle(2) << std::endl;
	std::cout << "\tPerimeter: " << tri.perimeter() << std::endl;
	std::cout << "\tArea: " << tri.area() << std::endl;
	std::cout << std::endl;
}

int main(int argc, char **argv)
{
	Triangle tri(3, 4, 5);
	Isosceles iso(10, 15);

	infoOutput(tri);
	infoOutput(iso);
	
	return 0;
}
