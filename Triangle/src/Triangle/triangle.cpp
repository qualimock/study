#include "triangle.h"

#include <exception>
#include <cmath>
#include <iostream>

Triangle::Triangle()
	: m_sides{1, 1, 1}
{}

Triangle::Triangle(double side1, double side2, double side3)
	: m_sides{side1, side2, side3}
{
	if (!isExist())
	{
		std::cout << "ERROR::TRIANGLE::TRIANGLE: Triangle(" << side1 << ", " << side2 << ", " << side3 << ") does not exist!" << std::endl;
		throw std::exception();
	}
}

bool Triangle::isExist()
{
	if (m_sides[0] > 0 && m_sides[1] > 0 && m_sides[2] > 0 &&
		m_sides[0] + m_sides[1] > m_sides[2] &&
		m_sides[0] + m_sides[2] > m_sides[1] &&
		m_sides[1] + m_sides[2] > m_sides[0])
	{
		return true;
	}

	return false;
}

double Triangle::perimeter() const noexcept
{
	return m_sides[0] + m_sides[1] + m_sides[2];
}

double Triangle::area() const noexcept
{
	double p = perimeter() / 2;
	return sqrt(p * (p - m_sides[0]) * (p - m_sides[1]) * (p - m_sides[2]));
}

double Triangle::calculateAngle(double side1, double side2, double side3) const noexcept
{
	return acos(((side1*side1 + side2*side2 - side3*side3) / (2 * side1 * side2))) * 180/M_PI;
}

double Triangle::angle(unsigned sideIndex) const noexcept
{
	switch (sideIndex)
	{
	case 0:
		return calculateAngle(m_sides[1], m_sides[2], m_sides[0]);
		break;
	case 1:
		return calculateAngle(m_sides[0], m_sides[2], m_sides[1]);
		break;
	case 2:
		return calculateAngle(m_sides[1], m_sides[0], m_sides[2]);
		break;
	default:
		return 0;
	}
}
