#pragma once

class Triangle
{
	double m_sides[3];

	bool isExist();

	double calculateAngle(double side1, double side2, double side3) const noexcept;
public:
	Triangle();
	Triangle(double side1, double side2, double side3);
	
	double perimeter() const noexcept;
	double area() const noexcept;
	
	double side(unsigned index) const noexcept { return m_sides[index]; }
	double angle(unsigned sideIndex) const noexcept;
};
