#include <iostream>
#include <string>
#include <array>

class Animal
{
	std::string m_name;

public:
	Animal(const std::string &name)
		: m_name(name)
	{}
};

class BoundedArray
{
	int *numbers;

	class Set
	{
		// ...
	};

public:
	BoundedArray(unsigned size)
	{
		numbers = new int[size];
	}

	~BoundedArray()
	{
		delete numbers;
	}
};


class String
{
	char *itsString;

public:
	String(const char *str)
	{
		itsString = const_cast<char *>(str);
	}

	friend std::ostream & operator<<(std::ostream &stream, String &string);
	friend std::istream & operator>>(std::istream &stream, String &string);
};

std::ostream & operator<<(std::ostream &stream, String &string)
{
	stream << string.itsString;
	return stream;
}

std::istream & operator>>(std::istream &stream, String &string)
{
	char *buffer = new char;
	stream >> buffer;

	string.itsString = buffer;
	
	return stream;
}

class animal;

class animal
{
	int itsWeight;
	int itsAge;

public:
	int getWeight() const { return itsWeight; }
	int getAge() const { return itsAge; }

	friend void setValue(animal &theAnimal, int theWeight);
	friend void setValue(animal &theAnimal, int theWeight, int theAge);
};

// void setValue(animal &theAnimal)
// {
// 	class animal {};
// }

void setValue(animal &theAnimal, int theWeight)
{
	theAnimal.itsWeight = theWeight;
}

void setValue(animal &theAnimal, int theWeight, int theAge)
{
	theAnimal.itsWeight = theWeight;
	theAnimal.itsAge = theAge;
}

int main(int argc, char **argv)
{
	String str("hello");

	std::cout << str << std::endl;

	std::cin >> str;

	std::cout << str << std::endl;
	
	return 0;
}
